import { Zapatilla } from './../models/zapatilla';
import { Injectable } from '@angular/core';

@Injectable()

export class ZapatillaService {
  public zapatillas: Array<Zapatilla>;

  constructor() {
    this.zapatillas = [
      new Zapatilla('Nike Airmax', 'Nike', 'Rojas', 80, true),
      new Zapatilla('Reebook Classic', 'Reebook', 'Blanco', 80, true),
      new Zapatilla('Reebook Spartan', 'Reebook', 'Negra', 180, true),
      new Zapatilla('Nike Runner MD', 'Nike', 'Negras', 60, true),
      new Zapatilla('Adidas Yezzy', 'Adidas', 'Gris', 180, false)
    ];
  }
  getText() {
    return 'Hola mundos desde un servicio';
  }
  getZapatilas(): Array<Zapatilla> {
    return this.zapatillas;
  }
}
