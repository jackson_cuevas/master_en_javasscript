import { ZapatillaService } from './../service/zapatilla.service';
import { Component, OnInit } from '@angular/core';
import { Zapatilla } from '../models/zapatilla';

@Component({
    selector: 'app-zapatillas',
    templateUrl: './zapatillas.component.html',
    providers: [ZapatillaService]
})

export class ZapatillasComponent implements OnInit {
    public titulo: String = 'Componente de zapatillas';
    public zapatillas: Array<Zapatilla>;
    public marcas: String[];
    public color: String;
    public mi_marca: String;


    constructor( private _zapatillaService: ZapatillaService
      ) {
        this.mi_marca = 'Fila';
        this.color = 'blue';
        this.marcas = new Array();
    }

    ngOnInit() {
        this.zapatillas = this._zapatillaService.getZapatilas();
        //alert(this._zapatillaService.getText());
        this.getMarcas();
    }

    getMarcas() {
        this.zapatillas.forEach((zapatilla, index) => {

            if (this.marcas.indexOf(zapatilla.marca) < 0) {
                this.marcas.push(zapatilla.marca);
            }
        });
        console.log(this.marcas);
    }

    getMarca() {
        alert(this.mi_marca);
    }

    setMarca() {
        this.marcas.push(this.mi_marca);
    }

    borrarMarca(index) {
        // delete this.marcas[index];
        this.marcas.splice(index, 1);
    }

    onBlur() {
        console.log('Has salido del input');
    }

    mostrarPalabra() {
        alert(this.mi_marca);
    }
}
