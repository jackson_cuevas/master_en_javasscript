$(document).ready(() => {
    //Selectro de ID
    $("#rojo").css("background","red")
              .css("color","white");
    $("#amarillo").css("background","yellow")
                  .css("color","green");
    $("#verde").css("background","green")
                .css("color","white");
    //Selector de clases
    var mi_clase = $('.zebra');
    mi_clase.css("border","5px dashed black");

    $('.sin_borde').click(function() {
        $(this).addClass('zebra');
    });
});