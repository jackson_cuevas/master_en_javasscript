import { Component, OnInit } from '@angular/core';
import { ViewChild } from 'node_modules.1/@angular/core';
declare var $: any;

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  public widthSlider: number;
  public anchuraToSlider: number;
  public captions: boolean;
  public autor: any;

  @ViewChild('textos') textos;

  constructor() {
    this.captions = false;
  }

  ngOnInit() {

    // var opcion_clasica = document.querySelector('#texto').innerHTML;
    alert(this.textos.nativeElement.innerText);

  }

  cargarSlider() {
    this.anchuraToSlider = this.widthSlider;
  }

  resetearSlider() {
    this.anchuraToSlider = null;
  }

  getAutor(event) {
    this.autor = event;
  }
}
